import React, { Component } from 'react'
import { getLogger } from "../core/utils"
import { Message } from './Message';
import { Text, View, TextInput, ScrollView, ActivityIndicator, TouchableOpacity, StyleSheet, Button, KeyboardAvoidingView, AsyncStorage } from 'react-native'
import { httpApiUrl } from '../core/api'
import WS from 'react-native-websocket'
import axios from 'axios'

const log = getLogger('Messages:');

export default class Messages extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            issue: null,
            messages: null,
            users: null
        };
    }

    async componentDidMount() {
        this.setState({ isLoading: true, issue: null });

        axios(`${httpApiUrl}/message`, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            timeout: 30000
        })
            .then((response) => {
                var messages = {};
                let data = response.data;
                data.forEach(message => {
                    if (messages[message.sender] == null) {
                        messages[message.sender] = [];
                    }
                    messages[message.sender].push({
                        id: message.id,
                        text: message.text,
                        read: message.read,
                        created: message.created,
                        sended: false
                    });
                });

                this.setState({ messages: messages, users: Object.keys(messages), isLoading: false });

                setInterval(this.sendReaded, 10000);
            })
            .catch((error) => {
                alert("Something went wrong" + error);
                this.setState({ issue: error, isLoading: false })
            });
    }

    sendReaded = () => {
        this.state.users.forEach(user => {
            this.state.messages[user]
                .filter(message => !message.sended && message.read)
                .forEach(message => {
                    fetch(`${httpApiUrl}/message/` + message.id, {
                        method: 'PUT',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            id: message.id,
                            read: true
                        }),
                    })
                        .then((response) => {

                            let newMessage = JSON.parse(response._bodyInit);
                            if (newMessage.read) {
                                message.sended = true;
                            }
                        })
                        .catch((error) => log(error));
                })

        })
    }

    receivedMessages = (user) => {
        var readedMessages = [];
        this.state.messages[user].forEach(message => {
            message.read = true;
            readedMessages.push(message);
        });

        this.state.messages[user] = readedMessages;

    }
    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View style={{ padding: 20 }}>
                    <Text style={styles.titleText}>MESAJE</Text>
                </View>

                <View style={{ padding: 20 }}>
                    <Text style={styles.buttonText}>Useri:</Text>
                    {this.state.isLoading
                        ? <ActivityIndicator size="large" color="#ffffff" />
                        : <ScrollView>
                            {this.state.users.map(user => <Message key={user} user={user} messages={this.state.messages[user]} receivedMessages={(user) => this.receivedMessages(user)} />)}
                        </ScrollView>
                    }
                </View>

                <WS
                    ref={ref => { this.ws = ref }}
                    url='http://192.168.0.64:3000'
                    onOpen={(data) => {
                        log("open success");
                    }}
                    onMessage={(e) => {
                        let message = JSON.parse(e.data);
                        if (!this.state.isLoading)
                            this.state.messages[message.sender].push({
                                id: message.id,
                                text: message.text,
                                read: message.read,
                                created: message.created,
                                sended: false
                            })
                    }}
                    onError={console.log}
                    onClose={console.log}
                    reconnect // Will try to reconnect onClose 
                ></WS>
            </KeyboardAvoidingView>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2c3e50',
    },
    titleText: {
        fontSize: 35,
        textAlign: 'left',

        margin: 10,
        color: '#ffffff'
    },
    formContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    input: {
        height: 40,
        backgroundColor: 'rgba(225,225,225,0.2)',
        marginBottom: 10,
        padding: 10,
        color: '#fff'
    },
    buttonContainer: {
        borderRadius: 10,
        backgroundColor: '#2980b6',
        paddingVertical: 15
    },
    buttonText: {
        color: '#ffffff',
        textAlign: 'center',
        fontWeight: '700',
    }
});

