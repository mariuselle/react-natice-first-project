import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { getLogger } from "../core/utils"

const log = getLogger('Messages:');

export class Message extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false
        }
    }
    render() {
        return (
            <View>
                <Text style={styles.userText} onPress={() => { this.setState({ show: true }); this.props.receivedMessages(this.props.user)}}> {this.props.user} </Text>
                {!this.state.show
                    ? <View />
                    : <View>
                        {this.props.messages.map(message => <Text key={message.id} style={styles.answerText}>- {message.text}</Text>)}
                    </View>
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    userText: {
        color: '#ffffff',
        textAlign: 'center',
        fontWeight: '700',
        padding: 5
    },
    answerText: {
        textAlign: 'center',
        fontWeight: '700',
        color: '#f3e5f5' 
    }


})
