export const apiUrl = '192.168.0.64:3000';
export const httpApiUrl = `http://${apiUrl}`;
export const wsApiUrl = `ws://${apiUrl}`;
export const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
};
